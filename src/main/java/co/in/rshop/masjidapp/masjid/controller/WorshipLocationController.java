package co.in.rshop.masjidapp.masjid.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.in.rshop.masjidapp.masjid.entities.Doner;
import co.in.rshop.masjidapp.masjid.entities.Member;
import co.in.rshop.masjidapp.masjid.entities.PrayerTime;
import co.in.rshop.masjidapp.masjid.entities.WorshipLocation;
import co.in.rshop.masjidapp.masjid.repositories.DonerRepo;
import co.in.rshop.masjidapp.masjid.repositories.MemberRepo;
import co.in.rshop.masjidapp.masjid.repositories.PrayerTimeRepo;
import co.in.rshop.masjidapp.masjid.repositories.WorshipLocationRepo;

@RestController
@RequestMapping("/api")
public class WorshipLocationController {
	@Autowired
	private WorshipLocationRepo worshipLocationRepo;
	@Autowired
	private MemberRepo memberRepo;
	@Autowired
	private PrayerTimeRepo prayerTimeRepo;
	@Autowired
	private DonerRepo donerRepo;

	// private String currentTime = "00:00"; // Initial time

	@PostMapping("/worshiplocations")
	public ResponseEntity<List<WorshipLocation>> createWorshipLocations(
			@RequestBody List<WorshipLocation> worshipLocations) {
		List<WorshipLocation> savedWorshipLocations = worshipLocationRepo.saveAll(worshipLocations);

		return ResponseEntity.ok(savedWorshipLocations);

	}

	@GetMapping("/worshiplocations")
	public ResponseEntity<List<WorshipLocation>> getWorshipLocations() {

		return ResponseEntity.ok(worshipLocationRepo.findAll());

	}

	@PostMapping("/worshiplocation")
	public ResponseEntity<WorshipLocation> createWorshipLocations(@RequestBody WorshipLocation worshipLocations) {
		WorshipLocation savedWorshipLocations = worshipLocationRepo.save(worshipLocations);

		return ResponseEntity.ok(savedWorshipLocations);
	}

//	@PutMapping("/worshiplocations/{id}")
//	public ResponseEntity<WorshipLocation> updateWorshipLocation(
//	        @PathVariable Long id,
//	        @RequestBody WorshipLocation updatedWorshipLocation) {
//	    return worshipLocationRepo.findById(id)
//	            .map(existingWorshipLocation -> {
//	                updatedWorshipLocation.setId(id);
//	                return ResponseEntity.ok(worshipLocationRepo.save(updatedWorshipLocation));
//	            })
//	            .orElse(ResponseEntity.notFound().build());
//	}
	@PutMapping("/worshiplocations/{id}")
	public ResponseEntity<WorshipLocation> updateWorshipLocation(@PathVariable Long id,
			@RequestBody Map<String, String> updatedPrayerTimings) {

		return worshipLocationRepo.findById(id).map(existingWorshipLocation -> {
			List<PrayerTime> prayerTimes = existingWorshipLocation.getPrayerTimes();

			// Iterate through the updated prayer timings map
			for (Map.Entry<String, String> entry : updatedPrayerTimings.entrySet()) {
				String prayerName = entry.getKey();
				String updatedTime = entry.getValue();

				// Find the PrayerTime entity with the matching prayer name
				for (PrayerTime prayerTime : prayerTimes) {
					if (prayerTime.getPrayerName().equalsIgnoreCase(prayerName)) {
						// Update the prayer time
						LocalTime parsedTime = parseTime(updatedTime);
						if (parsedTime != null) {
							// Set the current date for the prayer time
							prayerTime.setDate(LocalDate.now());

							// Update the prayer time
							prayerTime.setPrayerTime(parsedTime);
							// You may need to adjust this part based on your requirements
						} else {
							// Handle parsing error
							// You can choose to log the error or handle it as needed
						}
						break;
					}
				}
			}

			// Save the updated WorshipLocation entity
			return ResponseEntity.ok(worshipLocationRepo.save(existingWorshipLocation));
		}).orElse(ResponseEntity.notFound().build());
	}

	// Helper method to parse time string in "HH:mm" format to LocalTime object
	private LocalTime parseTime(String timeString) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
			return LocalTime.parse(timeString, formatter);
		} catch (DateTimeParseException e) {
			// Handle parsing exception
			e.printStackTrace();
			return null;
		}
	}

	@GetMapping("/worshiplocations/{id}")
	public ResponseEntity<WorshipLocation> getWorshipLocation1(@PathVariable Long id) {
		WorshipLocation worshipLocation = worshipLocationRepo.findById(id).orElse(null);
		if (worshipLocation != null) {
			return ResponseEntity.ok(worshipLocation);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// Other endpoints for managing prayer times, donors, etc.
	@PostMapping("/worships")
	public WorshipLocation createNewWorshipLocation(@RequestBody WorshipLocation worshipLocation) {
		worshipLocationRepo.save(worshipLocation);
		return worshipLocation;
	}

	@PostMapping("/member")
	public Member creareNewMember(@RequestBody Member member) {
		memberRepo.save(member);
		return member;
	}

	@PostMapping("/prayer")
	public PrayerTime createNewPrayerTime(@RequestBody PrayerTime prayerTime) {
		prayerTimeRepo.save(prayerTime);
		return prayerTime;

	}

	@PostMapping("/doner")
	public Doner createNewDoner(@RequestBody Doner doner) {
		donerRepo.save(doner);
		return doner;
	}

	@GetMapping("/worships/{id}")
	public ResponseEntity<WorshipLocation> getWorshipLocation(@PathVariable Long id) {
		WorshipLocation worshipLocation = worshipLocationRepo.findById(id).orElse(null);
		if (worshipLocation != null) {
			return ResponseEntity.ok(worshipLocation);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("/prayer/{id}")
	public ResponseEntity<PrayerTime> getPrayerTimEntity(@PathVariable Long id) {
		PrayerTime prayerTime = prayerTimeRepo.findById(id).orElse(null);
		if (prayerTime != null) {
			return ResponseEntity.ok(prayerTime);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
//   @GetMapping("/doner/{id}")
//   public ResponseEntity<Doner> getDonerEntity(@PathVariable Long id){
//	   Doner doner = donerRepo.findById(id).orElse(null);
//	   if(doner != null) {
//		   return ResponseEntity.ok(doner);
//	   }else {
//		   return ResponseEntity.notFound().build();
//	   }
//   }

}
