package co.in.rshop.masjidapp.masjid.entities;

import java.time.LocalDate;
import java.time.LocalTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class PrayerTime {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String prayerName; // eg,. Fajar, Zohar,Asra, Magrib,Ishaa etc,.
	private LocalTime azanTime;
	private LocalTime prayerTime;
	private LocalDate date;
	private String imamName;
	private String mosqueLocation;// location or name of the mosque

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrayerName() {
		return prayerName;
	}

	public void setPrayerName(String prayerName) {
		this.prayerName = prayerName;
	}

	public LocalTime getAzanTime() {
		return azanTime;
	}

	public void setAzanTime(LocalTime azanTime) {
		this.azanTime = azanTime;
	}

	public LocalTime getPrayerTime() {
		return prayerTime;
	}

	public void setPrayerTime(LocalTime prayerTime) {
		this.prayerTime = prayerTime;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getImamName() {
		return imamName;
	}

	public void setImamName(String imamName) {
		this.imamName = imamName;
	}

	public String getMosqueLocation() {
		return mosqueLocation;
	}

	public void setMosqueLocation(String mosqueLocation) {
		this.mosqueLocation = mosqueLocation;
	}

	public PrayerTime() {
		super();
		// TODO Auto-generated constructor stub
	}

}
