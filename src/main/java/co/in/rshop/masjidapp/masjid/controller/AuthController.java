package co.in.rshop.masjidapp.masjid.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.in.rshop.masjidapp.masjid.entities.User;
import co.in.rshop.masjidapp.masjid.repositories.UserRepo;

@RestController
@RequestMapping("/api")
public class AuthController {
	@Autowired
	private UserRepo userRepo;

	@PostMapping("/signup")
	public String signUp(@RequestBody User user) {
		if (userRepo.findByEmail(user.getEmail()) != null) {
			return "User with this email already exists";
		}

		userRepo.save(user);
		return "User signed up successfully!";
	}

	@PostMapping("/signin")
	public String signIn(@RequestBody User user) {
		// Find user by email
		User existingUser = userRepo.findByEmail(user.getEmail());

		// Check if user exists and password matches
		if (existingUser == null || !existingUser.getPassword().equals(user.getPassword())) {
			return "Invalid email or password!";
		}

		// User signed in successfully
		return "User signed in successfully!";
	}

	@GetMapping("/user/{email}")
	public User getUserByEmail(@PathVariable String email) {
		return userRepo.findByEmail(email);
	}

}
