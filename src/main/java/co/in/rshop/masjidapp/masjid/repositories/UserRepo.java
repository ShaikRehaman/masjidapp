package co.in.rshop.masjidapp.masjid.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.in.rshop.masjidapp.masjid.entities.User;

@Repository
public interface UserRepo  extends JpaRepository<User, Long>{
	
	User findByEmail(String email);

}
