package co.in.rshop.masjidapp.masjid.entities;

import java.math.BigDecimal;
import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Doner {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;
	private BigDecimal monthlyDonation;
	private Date donationDate;
	private String email;
	private String mobileNumber;
	private String address;
	private String occupation;
	private String donationPurpose; // Specify the purpose for the donation
	private String preferredPaymentMethod;// Prefeered donar payment method(eg,. Credit card,Bank transfer,check,
											// Money,Upi Payments)

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getMonthlyDonation() {
		return monthlyDonation;
	}

	public void setMonthlyDonation(BigDecimal monthlyDonation) {
		this.monthlyDonation = monthlyDonation;
	}

	public Date getDonationDate() {
		return donationDate;
	}

	public void setDonationDate(Date donationDate) {
		this.donationDate = donationDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getDonationPurpose() {
		return donationPurpose;
	}

	public void setDonationPurpose(String donationPurpose) {
		this.donationPurpose = donationPurpose;
	}

	public String getPreferredPaymentMethod() {
		return preferredPaymentMethod;
	}

	public void setPreferredPaymentMethod(String preferredPaymentMethod) {
		this.preferredPaymentMethod = preferredPaymentMethod;
	}

	public Doner() {
		super();
		// TODO Auto-generated constructor stub
	}

}
