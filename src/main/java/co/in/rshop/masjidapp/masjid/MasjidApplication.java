package co.in.rshop.masjidapp.masjid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication //(exclude = {SecurityAutoConfiguration.class})
 public class MasjidApplication extends SpringBootServletInitializer{
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MasjidApplication.class);
    }

	public static void main(String[] args) {
		SpringApplication.run(MasjidApplication.class, args);
	}
	

}