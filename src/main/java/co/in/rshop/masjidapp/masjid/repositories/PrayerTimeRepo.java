package co.in.rshop.masjidapp.masjid.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.in.rshop.masjidapp.masjid.entities.PrayerTime;

@Repository
public interface PrayerTimeRepo extends JpaRepository<PrayerTime, Long> {

}
